/**
 * 全局样式
 */

 import { Dimensions } from 'react-native';
 const { height, width } = Dimensions.get('window');

 module.exports={
     line:{
         width: window.width,
         height: 0.4,
         opacity: 0.5,
         backgroundColor: 'darkgray',
     },
     root_container: {
         flex: 1,
         backgroundColor: '#F3F3F3',
     },
     backgroundColor: '#2196F3',
     nav_bar_height_ios: 44,
     nav_bar_height_android: 50,
     window_height: height,
     window_width: width,
 }