import { resolve } from "dns";

export default class HttpUtils {
    static get(url) {
        return Promise((resolve, reject)=>{
            fetch(url)
            .then(response=>response.json)
            .then(result=>{
                resolve(result);
            })
            .catch(err=>{
                reject(err);
            })
        })
    }

    static post(url, data) {
        return Promise((resolve, reject)=>{
            fetch(url, {
                method: 'POST',
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json',
                },
                body: JSON.stringify(data)
            })
            .then(response=>response.json)
            .then(result=>{
                resolve(result);
            })
            .catch(err=>{
                reject(err);
            })
        })
    }
}