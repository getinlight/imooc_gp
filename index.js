import { AppRegistry } from 'react-native';
import AppNavigator from './js/navigators/AppNavigator';
import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings([
  'Warning: componentWillMount is deprecated',
  'Warning: componentWillReceiveProps is deprecated',
]);

function setup() {
    return AppNavigator;
}

AppRegistry.registerComponent('imooc_gp', () => setup());
