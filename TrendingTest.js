import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Navigator,
  AsyncStorage,
  TextInput
} from 'react-native';
import NavigationBar from './js/common/NavigationBar';
import Toast, { DURATION } from 'react-native-easy-toast';
import GitHubTrending from 'GitHubTrending';

const URL = 'https://github.com/trending/';

const KEY='text';

export default class TrendingTest extends Component {
    constructor(props) {
        super(props);
        this.trending = new GitHubTrending();
        this.state = {
            result: '',
        };
    }

    onLoad() {
        let url = URL+this.text;
        this.trending.fetchTrending(url)
            .then(result=>{
                console.log(result);
                this.setState({
                    result: JSON.stringify(result),
                });
            })
            .catch(err=>{
                this.setState({
                    result: JSON.stringify(err),
                });
            })
    }

    render() {
        return <View style={styles.container}>
            <NavigationBar
                title='GithubTrending'
                style={{backgroundColor:'#6495ED'}}
            ></NavigationBar>
            <TextInput style={{borderWidth:1, height: 40}}
                onChangeText={text=>this.text=text}
            ></TextInput>
            <View style={{flexDirection: 'row'}}>
                <Text style={styles.tips}
                    onPress={()=>this.onLoad()}
                >加载数据</Text>

            </View>
            <Text style={styles.text}>{this.state.result}</Text>
            <Toast ref={toast=>this.toast=toast}></Toast>
        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF',
    },
    tips: {
      fontSize: 15,
      margin: 5
    },
    page2: {
      flex: 1,
      backgroundColor: 'yellow',
    },
    image: {
      height: 22,
      width: 22,
    },
    text: {
        fontSize: 20,
        margin: 10,
    }
  });