import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Navigator,
  AsyncStorage,
  TextInput,
  WebView, 
  DeviceEventEmitter
} from 'react-native';
import NavigationBar from './js/common/NavigationBar';

const URL = 'https://www.imooc.com/';

export default class WebViewTest extends Component {

    constructor(props) {
        super(props);
        this.state={
            url: URL,
            title: '',
            canGoBack: false,
        };
    }

    goBack() {
        if (this.state.canGoBack) {
            this.webView.goBack();
        } else {
            DeviceEventEmitter.emit('showToast','到顶了');
        }
    }

    go() {
        this.setState({
            url: this.text,
        });
    }

    onNavigationStateChange(e) {
        this.setState({
            canGoBack: e.canGoBack,
            title: e.title,
        });
    }

    render() {
        return <View style={styles.container}>
            <NavigationBar
                title='webview的使用'
                style={{backgroundColor:'#6495ed'}}
            ></NavigationBar>
            <View style={styles.row}>
                <Text
                    onPress={()=>{
                        this.goBack();
                    }}
                >返回</Text>
                <TextInput style={styles.input}
                    defaultValue={URL}
                    onChangeText={text=>{
                        this.setState(text=>this.text=text);
                    }}
                ></TextInput>
                <Text
                    onPress={()=>{
                        this.go();
                    }}
                >前往</Text>
            </View>
            <WebView
                ref={webView=>this.webView=webView}
                source={{uri: this.state.url}}
                onNavigationStateChange={(e)=>this.onNavigationStateChange(e)}
            ></WebView>
        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF',
    },
    tips: {
      fontSize: 15,
      margin: 5
    },
    page2: {
      flex: 1,
      backgroundColor: 'yellow',
    },
    image: {
      height: 22,
      width: 22,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 10,
    },
    input: {
        height: 40,
        flex:1,
        borderWidth: 1,
        margin: 2,
    }
  });