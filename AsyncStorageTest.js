import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  AsyncStorage,
  TextInput
} from 'react-native';
import NavigationBar from './js/common/NavigationBar';
import Toast, { DURATION } from 'react-native-easy-toast';

const KEY='text';

export default class AsyncStorageTest extends Component {
    constructor(props) {
        super(props);
    }

    onSave() {
        AsyncStorage.setItem(KEY, this.text, (err)=>{
            if (!err) {
                this.toast.show('保存成功', DURATION.LENGTH_LONG);
            } else {
                this.toast.show('保存失败', DURATION.LENGTH_LONG);
            }
        })
    }

    onRemove() {
        AsyncStorage.removeItem(KEY, (err)=>{
            if (!err) {
                this.toast.show('删除成功', DURATION.LENGTH_LONG);
            }
        })
    }

    onFetch() {
        AsyncStorage.getItem(KEY, (err, result)=>{
            if (!err) {
                this.toast.show(result, DURATION.LENGTH_LONG);
            } else {
                this.toast.show(err, DURATION.LENGTH_LONG);
            }
        })
    }

    render() {
        return <View style={styles.container}>
            <NavigationBar
                title='AsyncStorageTest'
                style={{backgroundColor:'#6495ED'}}
            ></NavigationBar>
            <TextInput style={{borderWidth:1, height: 40}}
                onChangeText={text=>this.text=text}
            ></TextInput>
            <View style={{flexDirection: 'row'}}>
                <Text style={styles.tips}
                    onPress={()=>this.onSave()}
                >保存</Text>
                <Text style={styles.tips}
                    onPress={()=>this.onRemove()}
                >删除</Text>
                <Text style={styles.tips}
                    onPress={()=>this.onFetch()}
                >取出</Text>
            </View>
            <Toast ref={toast=>this.toast=toast}></Toast>
        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF',
    },
    tips: {
      fontSize: 15,
      margin: 5
    },
    page2: {
      flex: 1,
      backgroundColor: 'yellow',
    },
    image: {
      height: 22,
      width: 22,
    },
  });