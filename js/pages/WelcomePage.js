
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

import NavigationBar from '../common/NavigationBar';
import HomePage from './HomePage';
import ThemeDao from '../expand/dao/ThemeDao';
import NavigatorUtil from '../utils/NavigatorUtil';

export default class WelcomePage extends Component {

    componentDidMount() {
        new ThemeDao().getTheme().then(data=>{
            this.theme = data;
        })
        this.timer = setTimeout(() => {
            NavigatorUtil.resetToHomePage({
                theme: this.theme,
                navigation: this.props.navigation
            })
        }, 500);
    }

    componentWillUnmount() {
        this.timer&&clearTimeout(this.timer);
    }

    render() {
        return <View>
            <NavigationBar
                title={'欢迎'}
            ></NavigationBar>
            <Text>欢迎</Text>
        </View>
    }
}