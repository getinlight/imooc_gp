import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Navigator,
  AsyncStorage,
  TextInput,
  WebView, 
  DeviceEventEmitter,
  TouchableOpacity
} from 'react-native';
import NavigationBar from '../common/NavigationBar';
import ViewUtils from '../utils/ViewUtils';
import FavoriteDao from '../expand/dao/FavoriteDao';
import NavigatorUtil from '../utils/NavigatorUtil';
import BackPressComponent from '../common/BackPressComponent';
const TRENDING_URL = 'https://github.com/'

export default class RepositoryDetail extends Component {

    constructor(props) {
        super(props);
        this.backPress=new BackPressComponent({backPress:(e)=>this.onBackPress(e)});
        this.params = this.props.navigation.state.params;
        const { projectModel } = this.params
        this.url = projectModel.item.html_url ? projectModel.item.html_url
            : TRENDING_URL + projectModel.item.fullName;
        var title = projectModel.item.full_name ? projectModel.item.full_name
            : projectModel.item.fullName;
        this.favoriteDao = new FavoriteDao(this.params.flag);
        this.state = {
            url: this.url,
            canGoBack: false,
            title: title,
            isFavorite: projectModel.isFavorite,
            favoriteIcon: projectModel.isFavorite ? require('../../res/images/ic_star.png') : require('../../res/images/ic_star_navbar.png'),
        }
    }

    componentDidMount(){
        this.backPress.componentDidMount();
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        if (this.params.onUpdateFavorite) this.params.onUpdateFavorite();
    }

    onBackPress(e){
        this.onBack();
        return true;
    }

    onBack() {
        if (this.state.canGoBack) {
            this.webView.goBack();
        } else {
            NavigatorUtil.goBack(this.props.navigation);
        }
    }

    go() {
        this.setState({
            url: this.text,
        });
    }

    onNavigationStateChange(e) {
        this.setState({
            canGoBack: e.canGoBack,
        });
    }

    setFavoriteState(isFavorite) {
        this.setState({
            isFavorite: isFavorite,
            favoriteIcon: isFavorite ? require('../../res/images/ic_star.png') : require('../../res/images/ic_star_navbar.png')
        })
    }

    onRightButtonClick() {
        var projectModel = this.params.projectModel;
        this.setFavoriteState(projectModel.isFavorite=!projectModel.isFavorite);
        var key = projectModel.item.fullName ? projectModel.item.fullName : projectModel.item.id.toString();
        if(projectModel.isFavorite){
            this.favoriteDao.saveFavoriteItem(key, JSON.stringify(projectModel.item));
        } else {
            this.favoriteDao.removeFavoriteItem(key)
        }
    }

    renderRightButton() {
        return <TouchableOpacity
            onPress={()=>this.onRightButtonClick()}
        >
            <Image
                style={{width:20, height:20, marginRight:10}}
                source={this.state.favoriteIcon}
            />
        </TouchableOpacity>
    }

    render() {
        return <View style={styles.container}>
            <NavigationBar
                title={this.state.title}
                style={this.params.theme.styles.navBar}
                leftButton={ViewUtils.getLeftButton(()=>this.onBack())}
                rightButton={this.renderRightButton()}
            ></NavigationBar>
            <WebView
                ref={webView=>this.webView=webView}
                source={{uri: this.state.url}}
                onNavigationStateChange={(e)=>this.onNavigationStateChange(e)}
                startInLoadingState={true}
            ></WebView>
        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF',
    },
    tips: {
      fontSize: 15,
      margin: 5
    },
    page2: {
      flex: 1,
      backgroundColor: 'yellow',
    },
    image: {
      height: 22,
      width: 22,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 10,
    },
    input: {
        height: 40,
        flex:1,
        borderWidth: 1,
        margin: 2,
    }
  });