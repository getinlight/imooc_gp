import React, { Component } from 'react';
import {
  View,
  WebView,
} from 'react-native';

import NavigationBar from '../common/NavigationBar';
import GlobalStyles from '../../res/styles/GlobalStyles';
import ViewUtils from '../utils/ViewUtils';
import NavigatorUtil from '../utils/NavigatorUtil';

export default class WebViewPage extends Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state={
            url: this.params.url,
            title: this.params.title,
            canGoBack: false,
        };
    }

    onBackPress() {
        if (this.state.canGoBack) {
            this.webView.goBack();
        } else {
            NavigatorUtil.goBack(this.props.navigation);
        }
    }

    onNavigationStateChange(e) {
        this.setState({
            canGoBack: e.canGoBack,
            url: e.url,
        });
    }

    render() {
        return <View style={GlobalStyles.root_container}>
            <NavigationBar
                title={this.state.title}
                style={{backgroundColor:'#2196F3'}}
                leftButton={ViewUtils.getLeftButton(()=>this.onBackPress())}
            ></NavigationBar>
            <WebView
                ref={webView=>this.webView=webView}
                source={{uri: this.state.url}}
                startInLoadingState={true}
                onNavigationStateChange={(e)=>this.onNavigationStateChange(e)}
            ></WebView>
        </View>
    }
}
