import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Navigator,
  TextInput,
  ListView,
  RefreshControl,
  DeviceEventEmitter,
  Platform,
  TouchableOpacity,
  ActivityIndicator,
  FlatList
} from 'react-native';
import ViewUtils from '../utils/ViewUtils';
import GlobalStyles from '../../res/styles/GlobalStyles';
import Toast, { DURATION } from 'react-native-easy-toast';
import FavoriteDao from '../expand/dao/FavoriteDao';
import DataRepository, { FLAG_STORAGE } from '../expand/dao/DataRepository';
import ProjectModel from '../model/ProjectModel';
import Utils from '../utils/Utils';
import ActionUtils from '../utils/ActionUtils';
import RepositoryCell from '../common/RepositoryCell';
import LanguageDao, {FLAG_LANGUAGE} from '../expand/dao/LanguageDao';
import HomePage, {ACTION_HOME} from '../pages/HomePage';
import makeCancelable from '../utils/Cancelable';
import NavigatorUtil from '../utils/NavigatorUtil';
import BackPressComponent from '../common/BackPressComponent';

const API_URL = 'https://api.github.com/search/repositories?q=';
const QUERY_STR = '&sort=stars';

export default class SearchPage extends Component {

    constructor(props) {
        super(props);
        this.backPress=new BackPressComponent({backPress:(e)=>this.onBackPress(e)});
        this.params = this.props.navigation.state.params;
        this.favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_popular);
        this.favoriteKeys = [];
        this.keys = [];
        this.languageDao = new LanguageDao(FLAG_LANGUAGE.flag_key);
        this.isKeyChanged = false;
        this.state = {
          rightButtonText: '搜索',
          isLoading: false,
          showBottomButton: false,
          dataSource: []
        }
    }

    componentDidMount() {
      this.backPress.componentDidMount();
      this.initKeys();
    }

    onBackPress(e){
      this.onBack();
      return true;
    }

    componentWillUnmount() {
      this.backPress.componentWillUnmount();
      if(this.isKeyChanged) {
        DeviceEventEmitter.emit('ACTION_HOME', ACTION_HOME.RESTART)
      }
      this.cancelable&&this.cancelable.cancel();
    }

    /**
     * 添加标签
     */
    saveKey() {
      let key = this.inputKey;
      if (this.checkKeyIsExist(this.keys, key)) {
        this.toast.show(key+'已经存在');
      } else {
        key = {
          "path": key,
          "name": key,
          "checked": true
        };
        this.isKeyChanged = true;
        this.keys.unshift(key);
        this.languageDao.save(this.keys);
        this.toast.show(key.name+'保存成功', DURATION.LENGTH_LONG);
      }
    }

    /**
     * 获取所有标签
     */
    async initKeys() {
      this.keys = await this.languageDao.fetch();
    }

    /**
     * 检查key是否在keys里面
     * @param {*} keys 
     * @param {*} key 
     */
    checkKeyIsExist(keys, key) {
      for(let i = 0, l = keys.length; i<l;i++) {
        if(key.toLowerCase()===keys[i].name.toLowerCase()) {
          return true;
        }
      }
      return false;
    }

    genFetchUrl(key) {
      return API_URL + key + QUERY_STR;
    }

    /**
     * 获取本地用户收藏的ProjectItem
     */
    getFavoriteKeys() {
      this.favoriteDao.getFavoriteKeys()
      .then((keys)=>{
          this.favoriteKeys = keys||[];
          this.flushFavoriteState();
      })
      .catch((error)=>{
          this.flushFavoriteState();
          console.log(error);
      })
    }

    /**
     * 更新ProjectItem的Favorite状态
     */
    flushFavoriteState() {
      let projectModels = [];
      let items = this.items;
      for(var i = 0, len = items.length; i < len; i++) {
          projectModels.push(new ProjectModel(items[i], Utils.checkFavorite(items[i], this.favoriteKeys)));
      }
      this.updateState({
          isLoading: false,
          dataSource: projectModels,
          rightButtonText: '搜索',
      });
    }

    loadData() {
      this.updateState({
        isLoading: true,
      })
      this.cancelable = makeCancelable(fetch(this.genFetchUrl(this.inputKey)));
      this.cancelable.promise
      .then(response=>response.json())
      .then(responseData=>{
        if(!this||!responseData||!responseData.items||responseData.items.length===0) {
          this.toast.show(this.inputKey+'没有搜索结果', DURATION.LENGTH_LONG);
          this.updateState({isLoading: false, rightButtonText: '搜索'});
          return;
        }
        this.items=responseData.items;
        this.getFavoriteKeys();
        if(!this.checkKeyIsExist(this.keys, this.inputKey)) {
          this.updateState({showBottomButton: true})
        }
      })
      .catch(e=>{
        this.updateState({
          isLoading: false,
          rightButtonText: '搜索',
        })
      })
    }

    onBackPress() {
      this.refs.input.blur();
      NavigatorUtil.goBack(this.props.navigation);
    }

    updateState(dic) {
      this.setState(dic);
    }

    onRightButtonClick() {
      if (this.state.rightButtonText==='搜索') {
        this.loadData();
        this.updateState({
          rightButtonText: '取消',
        })
      } else {
        this.updateState({
          rightButtonText: '搜索',
          isLoading: false,
        })
        this.cancelable.cancel();
      }
    }

    renderNavBar() {
      let backButton = ViewUtils.getLeftButton(()=>this.onBackPress())
      let inputView = <TextInput 
        style={styles.textInput} 
        ref='input'
        onChangeText={text=>this.inputKey=text}></TextInput>
      let rightButton = <TouchableOpacity
        onPress={()=>{this.onRightButtonClick();this.refs.input.blur();}}
      >
        <View style={{marginRight: 10}}>
          <Text style={styles.title}>{this.state.rightButtonText}</Text>
        </View>
      </TouchableOpacity>
      return <View style={{
          backgroundColor: this.params.theme.themeColor,
          flexDirection: 'row', 
          alignItems: 'center',
          height: (Platform.OS==='ios')? GlobalStyles.nav_bar_height_ios: GlobalStyles.nav_bar_height_android,
      }}>
        {backButton}
        {inputView}
        {rightButton}
      </View>
    }

    onUpdateFavorite() {
      this.getFavoriteKeys();
    }

    renderItem(item) {
      const projectModel = item.item;
      return <RepositoryCell 
          key={projectModel.item.url} 
          projectModel={projectModel}
          theme={this.params.theme}
          onSelect={()=>ActionUtils.onSelectRepository({
              projectModel: projectModel,
              parentComponent: this,
              flag: FLAG_STORAGE.flag_popular,
              ...this.params,
              onUpdateFavorite:()=>this.onUpdateFavorite(),
          })}
          onFavorite={(item, isFavorite)=>ActionUtils.onFavorite(this.favoriteDao, item, isFavorite)}
      />
    }

    render() {
      let statusBar = null;
      if (Platform.OS === 'ios') {
        statusBar = <View style={[styles.statusBar, {backgroundColor: this.params.theme.themeColor}]}/>
      }
      let listView = this.state.isLoading?null:<FlatList
        data={this.state.dataSource}
        renderItem={(item)=>this.renderItem(item)}
        keyExtractor={item=>(item.item.id||item.item.fullName)+''}
      ></FlatList>
      let indicatorView = this.state.isLoading?
        <ActivityIndicator animating={this.state.isLoading} style={styles.centering} size='small'/> : null;
      let resultView = <View style={{flex: 1}}>
        {indicatorView}
        {listView}
      </View>
      let bottomButton = this.state.showBottomButton?
      <TouchableOpacity
        style={[styles.bottomButton, {backgroundColor: params.theme.themeColor}]}
        onPress={()=>this.saveKey()}
      >
        <View style={{justifyContent: 'center'}}>
          <Text style={styles.title}>添加标签</Text>
        </View>
      </TouchableOpacity>: null;
      return <View style={GlobalStyles.root_container}>
        {statusBar}
        {this.renderNavBar()}
        {resultView}
        {bottomButton}
        <Toast ref={toast=>this.toast=toast}/>
      </View>
    }

}

const styles = StyleSheet.create({
    statusBar: {
      height: 20,
    },
    textInput: {
      flex: 1,
      height: (Platform.OS==='ios')?30:40,
      borderWidth: (Platform.OS==='ios')?1:0,
      borderColor: 'white',
      alignSelf: 'center',
      paddingLeft: 5,
      marginRight: 10,
      marginLeft: 5,
      borderRadius: 3,
      opacity: 0.7,
      color: 'white',
    },
    title: {
      fontSize: 18,
      color: 'white',
      fontWeight: '500',
    },
    centering: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    bottomButton: {
      alignItems: 'center',
      justifyContent: 'center',
      opacity: 0.9,
      height: 40,
      position: 'absolute',
      left: 10,
      top: GlobalStyles.window_height - 45,
      right: 10,
      borderRadius: 3,
    }
});