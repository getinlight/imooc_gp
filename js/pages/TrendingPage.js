import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  ListView,
  RefreshControl,
  DeviceEventEmitter,
  TouchableOpacity,
  FlatList
} from 'react-native';

import NavigationBar from '../common/NavigationBar';
import DataRepository, { FLAG_STORAGE } from '../expand/dao/DataRepository';
import ScrollableTabView, {ScrollableTabBar} from 'react-native-scrollable-tab-view';
import LanguageDao, {FLAG_LANGUAGE} from '../expand/dao/LanguageDao';
import RepositoryDetail from './RepositoryDetail';
import TimeSpan from '../model/TimeSpan';
// import Popover from '../common/Popover';
import FavoriteDao from '../expand/dao/FavoriteDao';
import ProjectModel from '../model/ProjectModel';
import Utils from '../utils/Utils';
import TrendingCell from '../common/TrendingCell';
import ActionUtils from '../utils/ActionUtils';
import MoreMenu, {MORE_MENU} from '../common/MoreMenu';
import ViewUtils from '../utils/ViewUtils';
import { FLAG_TAB } from './HomePage';
import BaseComponent from './BaseComponent';
import CustomThemePage from './my/CustomTheme';

import TrendingDialog, {TimeSpans} from '../common/TrendingDialog';

const API_URL='https://github.com/trending/';

const EVENT_TYPE_SPAN_CHANGE = 'EVENT_TYPE_SPAN_CHANGE';

const dataRepository = new DataRepository(FLAG_STORAGE.flag_trending);
const favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_trending);

export default class TrendingPage extends BaseComponent {
    constructor(props) {
        super(props);
        this.languageDao = new LanguageDao(FLAG_LANGUAGE.flag_language);
        this.state = {
            languages: [],
            isVisible: false,
            timeSpan: TimeSpans[0],
            theme: this.props.theme,
            customThemeViewVisible: false,
        };
    }

    componentDidMount() {
        super.componentDidMount();
        this.loadLanguage();
    }

    loadLanguage() {
        this.languageDao.fetch()
        .then((languages)=>{
            this.setState({
                languages: languages
            })
        })
        .catch(err=>{
            console.log(err)
        })
    }

    showPopover() {
        this.dialog.show();
    }

    closePopover() {
        this.dialog.dismiss();
    }

    onSelectTimeSpan(timeSpan) {
        this.closePopover();
        DeviceEventEmitter.emit(EVENT_TYPE_SPAN_CHANGE, this.state.timeSpan, timeSpan);
        this.setState({
            timeSpan: timeSpan,
        });
    }

    renderTitleView() {
        return <View>
            <TouchableOpacity
                ref='button'
                onPress={()=>this.showPopover()}
            >
                <View style={{flexDirection: 'row', alignItems:'center'}}>
                    <Text style={{
                        fontSize: 18,
                        color:'white',
                        fontWeight:'400',
                    }}>趋势+{this.state.timeSpan.showText}</Text>
                    <Image 
                        style={{height:12, width:12, marginLeft: 5}} 
                        source={require('../../res/images/ic_spinner_triangle.png')}
                    />
                </View>
            </TouchableOpacity>
        </View>
    }

    renderMoreView() {
        let params = {...this.props, fromPage: FLAG_TAB.flag_favoriteTab}
        return <MoreMenu
            ref='moreMenu'
            {...params}
            menus={[
                MORE_MENU.Custom_Language,
                MORE_MENU.Sort_Language,
                MORE_MENU.Custom_Theme,
                MORE_MENU.About_Author,
                MORE_MENU.About
            ]}
            onMoreMenuSelect={(e)=>{
                if (e===MORE_MENU.Custom_Theme) {
                    this.setState({
                        customThemeViewVisible: true,
                    })
                }
            }}
        />
    }

    renderRightButton() {
        return <View style={{flexDirection: 'row'}}>
            {ViewUtils.getMoreButton(()=>this.refs.moreMenu.open())}
        </View>
    }

    renderCustomThemeView() {
        return (
          <CustomThemePage
            visible={this.state.customThemeViewVisible}
            {...this.props}
            onClose={()=>this.setState({
              customThemeViewVisible: false,
            })}
          ></CustomThemePage>
        );
    }

    renderTrendingDialog() {
        return (
            <TrendingDialog
                ref={dialog => this.dialog=dialog}
                onSelect={tab => this.onSelectTimeSpan(tab)}
            />
        );
    }

    render() {
        var statusBar = {
            backgroundColor: this.state.theme.themeColor,
        }
        let navigationBar = 
            <NavigationBar
                titleView = {this.renderTitleView()}
                statusBar={statusBar}
                style = {this.state.theme.styles.navBar}
                rightButton={this.renderRightButton()}
            />;
        let content = this.state.languages.length>0?
        <ScrollableTabView
            tabBarBackgroundColor={this.state.theme.themeColor}
            tabBarActiveTextColor='white'
            tabBarInactiveTextColor='mintcream'
            tabBarUnderlineStyle={{backgroundColor:'#e7e7e7', height: 2}}
            renderTabBar={()=><ScrollableTabBar/>}
        >
            {this.state.languages.map((result,i,arr)=>{
                return result.checked?<TrendingTab key={i} tabLabel={result.name} timeSpan={this.state.timeSpan} {...this.props}>{result.path}</TrendingTab>:null;
            })}
        </ScrollableTabView>:null;
        return (
        <View style={styles.container}>
            {navigationBar}
            {content}
            {this.renderMoreView()}
            {this.renderCustomThemeView()}
            {this.renderTrendingDialog()}
        </View>
        );
    }
}

class TrendingTab extends BaseComponent {
    constructor(props) {
        super(props);
        this.isFavoriteChanged=false;
        this.state = {
            projectModels: [],
            isLoading: false,
            favoriteKeys: [],
        };
    }

    componentDidMount() {
        super.componentDidMount();
        this.listener = DeviceEventEmitter.addListener('favoriteChanged_trending', ()=>{
            this.isFavoriteChanged=true;
        });
        this.timeSpanChangeListener = DeviceEventEmitter.addListener(EVENT_TYPE_SPAN_CHANGE, (from, to)=> {
            this.loadData(to);
        });
        this.loadData(this.props.timeSpan);
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        if (this.listener) {
            this.listener.remove();
        }
        if (this.homeTabSelectListener) {
            this.homeTabSelectListener.remove();
        }
    }

    onTabSelected(from, to) {
        if (to === FLAG_TAB.flag_trendingTab && this.isFavoriteChanged) {
            this.isFavoriteChanged = false;
            this.getFavoriteKeys();
        }
    }

    // componentWillReceiveProps(nextProps) {
    //     if (nextProps.timeSpan !== this.props.timeSpan) {
    //         this.loadData(nextProps.timeSpan, true);
    //     } else if (this.isFavoriteChanged) {
    //         this.isFavoriteChanged=false;
    //         this.loadData(nextProps.timeSpan, true);
    //     } else if(nextProps.theme !== this.state.theme){
    //         this.updateState({
    //             theme: nextProps.theme,
    //         });
    //         this.flushFavoriteState();
    //     }
    // }

    flushFavoriteState() {
        let projectModels = [];
        let items = this.items;
        for(let i=0, len=items.length;i<len;i++) {
            projectModels.push(new ProjectModel(items[i], Utils.checkFavorite(items[i], this.state.favoriteKeys)));
        }
        this.updateState({
            isLoading: false,
            projectModels: projectModels,
        });
    }

    getFavoriteKeys() {
        favoriteDao.getFavoriteKeys()
        .then((keys)=>{
            if(keys){
                this.updateState({favoriteKeys: keys})
            }
            this.flushFavoriteState()
        })
        .catch(e=>{
            this.flushFavoriteState()
        })
    }

    updateState(dict) {
        if(!dict) return;
        this.setState(dict);
    }

    genFetchUrl(timeSpan, category) {//objective-c?since=daily
        return API_URL + category + '?' + timeSpan.searchText;
    }

    loadData(timeSpan, isRefresh) {
        this.updateState({
            isLoading: true
        })
        let url = this.genFetchUrl(timeSpan, this.props.tabLabel);
        dataRepository.fetchRepository(url)
        .then((result)=>{
            this.items = result&&result.items?result.items:result?result:[];
            this.getFavoriteKeys();
            if(!this.items||isRefresh&&result && result.update_date && !Utils.checkDate(result.update_date)){
                return dataRepository.fetchNetRepository(url);
            }
        })
        .then(items=>{
            if (!items || items.length == 0) return;
            this.items=items;
            this.getFavoriteKeys();
        })
        .catch(err=>{
            this.updateState({
                isLoading: false,
            })
        })
    }

    onUpdateFavorite() {
        this.getFavoriteKeys();
    }

    renderItem(data) {
        const projectModel = data.item;
        return <TrendingCell
            key={projectModel.item.id}
            projectModel={projectModel}
            theme={this.props.theme}
            onSelect={()=>ActionUtils.onSelectRepository({
                projectModel: projectModel,
                parentComponent: this,
                flag: FLAG_STORAGE.flag_popular,
                ...this.props,
                onUpdateFavorite:()=>this.onUpdateFavorite(),
            })}
            onFavorite={(item, isFavorite)=>ActionUtils.onFavorite(favoriteDao, item, isFavorite, FLAG_STORAGE.flag_trending)}
        />
    }

    onRefresh() {
        this.loadData(this.props.timeSpan);
    }

    render() {
        return <View style={{flex: 1}}>
            <FlatList
                data={this.state.projectModels}
                renderItem={(data)=>this.renderItem(data)}
                keyExtractor={item=>(item.item.id||item.item.fullName)+''}
                refreshControl={<RefreshControl
                    refreshing={this.state.isLoading}
                    onRefresh={()=>this.onRefresh()}
                    colors={['#2196F3']}
                    tintColor={this.props.theme.themeColor}
                    title='loading...'
                    titleColor={this.props.theme.themeColor}
                />}
            ></FlatList>
        </View>
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    page1: {
        flex: 1,
        backgroundColor: 'red',
    },
    page2: {
        flex: 1,
        backgroundColor: 'yellow',
    },
    image: {
        height: 22,
        width: 22,
    },
});

