import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  ListView,
  RefreshControl,
  DeviceEventEmitter,
  FlatList
} from 'react-native';

import NavigationBar from '../common/NavigationBar';
import DataRepository, { FLAG_STORAGE } from '../expand/dao/DataRepository';
import ScrollableTabView, {ScrollableTabBar} from 'react-native-scrollable-tab-view';
import RepositoryCell from '../common/RepositoryCell';
import TrendingCell from '../common/TrendingCell';
import ProjectModel from '../model/ProjectModel';
import FavoriteDao from '../expand/dao/FavoriteDao';
import ArrayUtils from '../utils/ArrayUtils';
import ActionUtils from '../utils/ActionUtils';
import MoreMenu, {MORE_MENU} from '../common/MoreMenu';
import ViewUtils from '../utils/ViewUtils';
import { FLAG_TAB } from './HomePage';
import BaseComponent from './BaseComponent';
import CustomThemePage from './my/CustomTheme';

export default class FavoritePage extends BaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            theme: this.props.theme,
            customThemeViewVisible: false,
        };
    }

    renderRightButton() {
        return <View style={{flexDirection: 'row'}}>
            {ViewUtils.getMoreButton(()=>this.refs.moreMenu.open())}
        </View>
    }

    renderMoreView() {
        let params = {...this.props, fromPage: FLAG_TAB.flag_favoriteTab}
        return <MoreMenu
            ref='moreMenu'
            {...params}
            menus={[
                MORE_MENU.Custom_Theme,
                MORE_MENU.About_Author,
                MORE_MENU.About
            ]}
            anchorView={()=>this.refs.moreMenuButton}
            onMoreMenuSelect={(e)=>{
                if (e===MORE_MENU.Custom_Theme) {
                    this.setState({
                        customThemeViewVisible: true,
                    })
                }
            }}
        />
    }

    renderCustomThemeView() {
        return (
          <CustomThemePage
            visible={this.state.customThemeViewVisible}
            {...this.props}
            onClose={()=>this.setState({
              customThemeViewVisible: false,
            })}
          ></CustomThemePage>
        );
    }

    render() {
        var statusBar = {
            backgroundColor: this.state.theme.themeColor,
        }
        let navigationBar = 
            <NavigationBar
                title={'收藏'}
                statusBar={statusBar}
                style = {this.state.theme.styles.navBar}
                rightButton={this.renderRightButton()}
            />;
        let content = <ScrollableTabView
                tabBarBackgroundColor={this.state.theme.themeColor}
                tabBarActiveTextColor='white'
                tabBarInactiveTextColor='mintcream'
                tabBarUnderlineStyle={{backgroundColor:'#e7e7e7', height: 2}}
                renderTabBar={()=><ScrollableTabBar/>}
            >
                <FavoriteTab tabLabel='最热' flag={FLAG_STORAGE.flag_popular} {...this.props}/>
                <FavoriteTab tabLabel='趋势' flag={FLAG_STORAGE.flag_trending} {...this.props}/>
            </ScrollableTabView>
        return <View style={styles.container}>
            {navigationBar}
            {content}
            {this.renderMoreView()}
            {this.renderCustomThemeView()}
        </View>;
    }
}

class FavoriteTab extends BaseComponent {
    constructor(props) {
        super(props);
        this.favoriteDao = new FavoriteDao(this.props.flag);
        this.unFavoriteItems=[];
        this.state = {
            projectModels: [],
            isLoading: false,
            favoriteKeys: [],
            theme: this.props.theme,
        };
    }

    componentDidMount() {
        super.componentDidMount();
        this.loadData(true);
    }

    onTabSelected(from, to) {
        if (to === FLAG_TAB.flag_favoriteTab) { 
            this.loadData(false);
        }
    }

    loadData(isShowLoading) {
        if (isShowLoading) {
            this.updateState({
                isLoading: true
            })
        }
        this.favoriteDao.getAllItems()
        .then(items=>{
            var resultData = [];
            for(var i=0, len=items.length; i<len; i++) {
                resultData.push(new ProjectModel(items[i], true))
            }
            this.updateState({
                isLoading: false,
                projectModels: resultData,
            })
        })
        .catch(e=>{
            this.updateState({
                isLoading: false,
            })
        })
    }

    updateState(dic) {
        if (!this) return;
        this.setState(dic);
    }

    onUpdateFavorite() {
        this.getFavoriteKeys();
    }

    /**
     * 获取本地用户收藏的ProjectItem
     */
    getFavoriteKeys() {
        this.favoriteDao.getFavoriteKeys()
        .then((keys)=>{
            if(keys){
                this.updateState({favoriteKeys: keys});
            }
        })
        .catch((error)=>{

        })
    }

    /**
     * favoriteIcon单击回调函数
     * @param item
     * @param isFavorite
     */
    onFavorite(item, isFavorite) {
        ActionUtils.onFavorite(this.favoriteDao, item, isFavorite, this.props.flag);
        ArrayUtils.updateArray(this.unFavoriteItems, item);
        if (this.unFavoriteItems.length > 0) {
            if (this.props.flag === FLAG_STORAGE.flag_popular) {
                DeviceEventEmitter.emit('favoriteChanged_popular');
            } else {
                DeviceEventEmitter.emit('favoriteChanged_trending');
            }``
        }
    }

    renderRow(data) {
        const projectModel = data.item;
        let CellComponent = this.props.flag===FLAG_STORAGE.flag_popular?RepositoryCell:TrendingCell;
        return <CellComponent 
            key={this.props.flag===FLAG_STORAGE.flag_popular ? projectModel.item.id : projectModel.item.fullName}
            projectModel={projectModel}
            theme={this.props.theme}
            onSelect={()=>ActionUtils.onSelectRepository({
                projectModel: projectModel,
                parentComponent: this,
                flag: FLAG_STORAGE.flag_popular,
                ...this.props,
                onUpdateFavorite:()=>this.onUpdateFavorite(),
            })}
            onFavorite={(item, isFavorite)=>this.onFavorite(item, isFavorite)}
        />
    }

    render() {
        return <View style={styles.container}>
            <FlatList
                data={this.state.projectModels}
                enableEmptySections={true}
                renderItem={(data)=>this.renderRow(data)}
                keyExtractor={item=>this.props.flag===FLAG_STORAGE.flag_popular ? item.item.id+'' : item.item.fullName+''}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.isLoading}
                        onRefresh={()=>this.loadData()}
                        colors={['#2196F3']}
                        tintColor={'#2196F3'}
                        title='loading...'
                        titleColor={'#2196F3'}
                    />
                }
            ></FlatList>
        </View>
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

