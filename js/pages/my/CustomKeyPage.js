import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  ScrollView,
  Alert,
  DeviceEventEmitter
} from 'react-native';

import NavigationBar from '../../common/NavigationBar';
import ViewUtils from '../../utils/ViewUtils';
import LanguageDao, {FLAG_LANGUAGE} from '../../expand/dao/LanguageDao';
import CheckBox from 'react-native-check-box';
import ArrayUtils from '../../utils/ArrayUtils';
import {ACTION_HOME, FLAG_TAB} from '../HomePage';
import BackPressComponent from '../../common/BackPressComponent';
import NavigatorUtil from '../../utils/NavigatorUtil';

export default class CustomKeyPage extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.backPress=new BackPressComponent({backPress:(e)=>this.onBackPress(e)});
    this.isRemoveKey = this.params.isRemoveKey;
    this.languageDao = new LanguageDao(this.params.flag);
    this.changeValues=[];
    this.state = {
      dataArray:[]
    };
  }

  onBackPress(e){
    this.onBack();
    return true;
  }

  componentDidMount() {
    this.backPress.componentDidMount();
    this.loadData();
  }

  componentWillUnmount() {
    this.backPress.componentWillUnmount();
  }

  loadData() {
    this.languageDao.fetch()
    .then(result=>{
      this.setState({
        dataArray: result
      })
    })
    .catch(err=>{
      console.log(err)
    })
  }

  onSave() {
    if (this.changeValues.length !== 0) {
      this.languageDao.save(this.state.dataArray);
    }
    if (this.isRemoveKey) {
      for (let i = 0, l=this.changeValues.length;i<l;i++) {
        ArrayUtils.remove(this.state.dataArray, this.changeValues[i]);
      }
    }
    this.languageDao.save(this.state.dataArray);
    var jumpToTab = this.params.flag === FLAG_LANGUAGE.flag_key? FLAG_TAB.flag_popularTab: FLAG_TAB.flag_favoriteTab;
    DeviceEventEmitter.emit('ACTION_HOME', ACTION_HOME.RESTART, jumpToTab)
    NavigatorUtil.goBack(this.props.navigation);
  }

  renderView() {
    if (!this.state.dataArray||this.state.dataArray.length===0) return null;
    let len = this.state.dataArray.length;
    let views = [];
    for (let i=0, l=len-2; i<l; i+=2) {
      views.push(
        <View key={i}>
          <View style={styles.item}>
            {this.renderCheckBox(this.state.dataArray[i])}
            {this.renderCheckBox(this.state.dataArray[i+1])}
          </View>
          <View style={styles.line}></View>
        </View>
      );
    }
    views.push(
      <View key={len-1}>
        <View style={styles.item}>
            {len%2===0?this.renderCheckBox(this.state.dataArray[len-2]):null}
            {this.renderCheckBox(this.state.dataArray[len-1])}
          </View>
          <View style={styles.line}></View>
      </View>
    );
    return views;
  }

  renderCheckBox(data) {
    let leftText = data.name;
    let isChecked = this.isRemoveKey ? false: data.checked;
    return (
      <CheckBox
        style={{flex: 1, padding: 10}}
        onClick={()=>this.onClick(data)}
        leftText={leftText}
        isChecked={isChecked}
        checkedImage={<Image 
          style={{tintColor: this.params.theme.themeColor}}
          source={require('./img/ic_check_box.png')}/>}
        unCheckedImage={<Image 
          style={{tintColor: this.params.theme.themeColor}}
          source={require('./img/ic_check_box_outline_blank.png')}/>}
      />
    );
  }

  onClick(data) {
    if (!this.isRemoveKey) {
      data.checked=!data.checked;
    }
    ArrayUtils.updateArray(this.changeValues, data);
  }

  onBack() {
    if (this.changeValues.length === 0) {
      NavigatorUtil.goBack(this.props.navigation);
      return;
    }
    Alert.alert('提示','要保持修改吗?',
      [
        {text:'不保存', onPress:()=>{
          NavigatorUtil.goBack(this.props.navigation);
        }, style: 'cancel'},
        {text:'保存', onPress:()=>{
          this.onSave()
        }}
      ]
    );
  }

  render() {
    let title = this.isRemoveKey? '标签移除': '自定义标签';
    title=this.params.flag===FLAG_LANGUAGE.flag_language?'自定义语言':title;
    let rightButtonTitle = this.isRemoveKey? '移除': '保存';
    let rightButton=ViewUtils.getRightButton(rightButtonTitle, ()=>this.onSave());
    return (
      <View style={styles.container}>
        <NavigationBar
          title={title}
          leftButton={ViewUtils.getLeftButton(()=>this.onBack())}
          rightButton={rightButton}
          style={this.params.theme.styles.navBar}
        ></NavigationBar>
        <ScrollView>
          {this.renderView()}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 18,
    color: 'white',
  },
  page2: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  image: {
    height: 22,
    width: 22,
  },
  line: {
    height: 0.3,
    backgroundColor: 'darkgray'
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
  }
});
