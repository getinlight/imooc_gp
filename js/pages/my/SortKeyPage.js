import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Alert,
  TouchableHighlight,
  DeviceEventEmitter
} from 'react-native';

import NavigationBar from '../../common/NavigationBar';
import ViewUtils from '../../utils/ViewUtils';
import LanguageDao, {FLAG_LANGUAGE} from '../../expand/dao/LanguageDao';
import CheckBox from 'react-native-check-box';
import ArrayUtils from '../../utils/ArrayUtils';
import SortableListView from 'react-native-sortable-listview';
import {ACTION_HOME, FLAG_TAB} from '../HomePage';
import BackPressComponent from '../../common/BackPressComponent';
import NavigatorUtil from '../../utils/NavigatorUtil';

export default class SortKeyPage extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.backPress=new BackPressComponent({backPress:(e)=>this.onBackPress(e)});
    this.dataArray = []; //原始数组
    this.originalCheckedArray = []; //筛选后的数组
    this.sortResultArray = []; //排序结果应用到原始数组
    this.languageDao = new LanguageDao(this.params.flag);
    this.changeValues=[];
    this.state = {
      checkedArray: [] //筛选后的数组进行排序
    };
  }

  componentDidMount() {
    this.backPress.componentDidMount();
    this.loadData();
  }

  componentWillUnmount() {
    this.backPress.componentWillUnmount();
  }
  onBackPress(e){
      this.onBack();
      return true;
  }

  loadData() {
    this.languageDao.fetch()
    .then(result=>{
      this.getCheckedItems(result);
    })
    .catch(err=>{
      console.log(err)
    })
  }

  getCheckedItems(result) {
    this.dataArray = result;
    let checkedArray = [];
    for (let i = 0, len=result.length;i<len;i++) {
      let data = result[i];
      if (data.checked) checkedArray.push(data);
    }
    this.setState({
      checkedArray: checkedArray,
    });
    this.originalCheckedArray = ArrayUtils.clone(checkedArray);
  }

  onBack() {
    if (ArrayUtils.isEqual(this.originalCheckedArray, this.state.checkedArray)) {
      NavigatorUtil.goBack(this.props.navigation);
      return;
    }
    Alert.alert('提示','要保持修改吗?',
      [
        {text:'不保存', onPress:()=>{
          NavigatorUtil.goBack(this.props.navigation);
        }, style: 'cancel'},
        {text:'保存', onPress:()=>{
          this.onSave(true)
        }}
      ]
    );
  }

  onSave(isChecked) {
    if (!isChecked && ArrayUtils.isEqual(this.originalCheckedArray, this.state.checkedArray)) {
      NavigatorUtil.goBack(this.props.navigation);
      return;
    }
    this.getSortResult();
    this.languageDao.save(this.sortResultArray);
    var jumpToTab = this.params.flag === FLAG_LANGUAGE.flag_key? FLAG_TAB.flag_popularTab: FLAG_TAB.flag_favoriteTab;
    DeviceEventEmitter.emit('ACTION_HOME', ACTION_HOME.RESTART, jumpToTab)
    NavigatorUtil.goBack(this.props.navigation);
  }

  getSortResult() {
    this.sortResultArray = ArrayUtils.clone(this.dataArray);
    for (let i=0,l=this.originalCheckedArray.length;i<l;i++) {
      let item = this.originalCheckedArray[i];
      let index = this.dataArray.indexOf(item);
      this.sortResultArray.splice(index, 1, this.state.checkedArray[i]);
    }
  }

  renderView() {
    if (!this.state.dataArray||this.state.dataArray.length===0) return null;
    let len = this.state.dataArray.length;
    let views = [];
    for (let i=0, l=len-2; i<l; i+=2) {
      views.push(
        <View key={i}>
          <View style={styles.item}>
            {this.renderCheckBox(this.state.dataArray[i])}
            {this.renderCheckBox(this.state.dataArray[i+1])}
          </View>
          <View style={styles.line}></View>
        </View>
      );
    }
    views.push(
      <View key={len-1}>
        <View style={styles.item}>
            {len%2===0?this.renderCheckBox(this.state.dataArray[len-2]):null}
            {this.renderCheckBox(this.state.dataArray[len-1])}
          </View>
          <View style={styles.line}></View>
      </View>
    );
    return views;
  }

  renderCheckBox(data) {
    let leftText = data.name;
    return (
      <CheckBox
        style={{flex: 1, padding: 10}}
        onClick={()=>this.onClick(data)}
        leftText={leftText}
        isChecked={data.checked}
        checkedImage={<Image 
          style={{tintColor:'#6495ED'}}
          source={require('./img/ic_check_box.png')}/>}
        unCheckedImage={<Image 
          style={{tintColor:'#6495ED'}}
          source={require('./img/ic_check_box_outline_blank.png')}/>}
      />
    );
  }

  onClick(data) {
    data.checked=!data.checked;
    ArrayUtils.updateArray(this.changeValues, data);
  }

  render() {
    let rightButton=<TouchableOpacity
      onPress={()=>this.onSave()}
    >
      <View style={{margin: 10}}>
        <Text style={styles.title}>保存</Text>
      </View>
    </TouchableOpacity>
    let title = this.params.flag === FLAG_LANGUAGE.flag_key?'标签排序':'语言排序';
    return (
      <View style={styles.container}>
        <NavigationBar
          title={title}
          leftButton={ViewUtils.getLeftButton(()=>this.onBack())}
          rightButton={rightButton}
          style={this.params.theme.styles.navBar}
        ></NavigationBar>
        <SortableListView
          style={{ flex: 1 }}
          data={this.state.checkedArray}
          order={Object.keys(this.state.checkedArray)}
          onRowMoved={e => {
            this.state.checkedArray.splice(e.to, 0, this.state.checkedArray.splice(e.from, 1)[0])
            this.forceUpdate()
          }}
          renderRow={row => <SortCell data={row} {...this.params}/>}
        />
      </View>
    );
  }
}

class SortCell extends Component {
  render() {
    return (
      <TouchableHighlight
        underlayColor={'#eee'}
        delayLongPress={500}
        style={styles.item}
        {...this.props.sortHandlers}
      >
        <View style={styles.row}>
        <Image source={require('./img/ic_sort.png')} 
              resizeMode='stretch' 
              style={[{
                    opacity: 1,
                    width: 16,
                    height: 16,
                    marginRight: 10,
                }, this.props.theme.styles.tabBarSelectedIcon]}/>
          <Text>{this.props.data.name}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 18,
    color: 'white',
  },
  page2: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  line: {
    height: 0.3,
    backgroundColor: 'darkgray'
  },
  item: {
    padding:15, 
    backgroundColor:'#F8F8F8', 
    borderBottomWidth: 1, 
    borderColor: '#eee'
  },
  row: {
    flexDirection:'row',
    alignItems: 'center',
  }
});
