import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Navigator,
  ScrollView,
  TouchableHighlight
} from 'react-native';

import NavigationBar from '../../common/NavigationBar';
import {MORE_MENU} from '../../common/MoreMenu';
import GlobalStyles from '../../../res/styles/GlobalStyles';
import ViewUtils from '../../utils/ViewUtils';
import { FLAG_LANGUAGE } from '../../expand/dao/LanguageDao';
import CustomThemePage from '../my/CustomTheme';
import BaseComponent from '../BaseComponent';
import NavigatorUtil from '../../utils/NavigatorUtil';

export default class MyPage extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      customThemeViewVisible: false,
      theme: this.props.theme
    };
  }

  renderCustomThemeView() {
    return (
      <CustomThemePage
        visible={this.state.customThemeViewVisible}
        {...this.props}
        onClose={()=>this.setState({
          customThemeViewVisible: false,
        })}
      ></CustomThemePage>
    );
  }

  onClick(tab) {
    let TargetComponent, params = {...this.props,menuType:tab}
    switch (tab) {
      case MORE_MENU.Custom_Language:
          TargetComponent = 'CustomKeyPage';
          params.flag = FLAG_LANGUAGE.flag_language;
          break;
      case MORE_MENU.Custom_Key:
          TargetComponent = 'CustomKeyPage';
          params.flag = FLAG_LANGUAGE.flag_key;
          break;
      case MORE_MENU.Remove_Key:
          TargetComponent = 'CustomKeyPage';
          params.flag = FLAG_LANGUAGE.flag_key;
          break;
      case MORE_MENU.Sort_Language:
          TargetComponent = 'SortKeyPage';
          params.flag = FLAG_LANGUAGE.flag_language;
          break;
      case MORE_MENU.Sort_Key:
          TargetComponent = 'SortKeyPage';
          params.flag = FLAG_LANGUAGE.flag_key;
          break;
      case MORE_MENU.Custom_Theme:
          this.setState({
            customThemeViewVisible: true,
          })
          break;
      case MORE_MENU.About_Author:
          TargetComponent = 'AboutMePage';
          break;
      case MORE_MENU.About:
          TargetComponent = 'AboutPage';
          break;
    }
    if(TargetComponent) {
      // this.props.navigator.push({
      //   component: TargetComponent,
      //   params: params,
      // });
      NavigatorUtil.gotoMenuPage({...params}, TargetComponent);
    }
  }

  render() {
    let navigationBar = <NavigationBar
        style={this.state.theme.styles.navBar}
        title='我的'
      ></NavigationBar>;
    return (
      <View style={GlobalStyles.root_container}>
        {navigationBar}
        <ScrollView>
          <TouchableHighlight
            onPress={()=>this.onClick(MORE_MENU.About)}
          >
            <View style={styles.item}>
              <View style={{flexDirection: 'row', alignItems:'center'}}>
                <Image source={require('../../../res/images/ic_trending.png')}
                    style={[{width:40, height:40, marginRight:10}, {tintColor: this.props.theme.themeColor}]}
                />
                <Text>Github Popular</Text>
              </View>
              <Image source={require('../../../res/images/ic_tiaozhuan.png')}
                style={[{marginRight: 10, height: 22, width: 22}, {tintColor: this.props.theme.themeColor}]}
              />
            </View>
          </TouchableHighlight>
          <View style={GlobalStyles.line}/>
          {/* 趋势管理 */}
          <Text style={styles.groupTitle}>趋势管理</Text>
          <View style={GlobalStyles.line}/>
          {ViewUtils.getSettingItem(()=>this.onClick(MORE_MENU.Custom_Language),
            '自定义语言',
            require('./img/ic_custom_language.png'),
            {tintColor: this.props.theme.themeColor},
            null)}
          <View style={GlobalStyles.line}/>
          {ViewUtils.getSettingItem(()=>this.onClick(MORE_MENU.Sort_Language),
            '语言排序',
            require('./img/ic_swap_vert.png'),
            {tintColor: this.props.theme.themeColor},
            null)}
          <View style={GlobalStyles.line}/>
          {/* 标签管理 */}
          <Text style={styles.groupTitle}>标签管理</Text>
          <View style={GlobalStyles.line}/>
          {ViewUtils.getSettingItem(()=>this.onClick(MORE_MENU.Custom_Key),
            '自定义标签',
            require('./img/ic_custom_language.png'),
            {tintColor: this.props.theme.themeColor},
            null)}
          <View style={GlobalStyles.line}/>
          {ViewUtils.getSettingItem(()=>this.onClick(MORE_MENU.Sort_Key),
            '标签排序',
            require('./img/ic_swap_vert.png'),
            {tintColor: this.props.theme.themeColor},
            null)}
          <View style={GlobalStyles.line}/>
          {ViewUtils.getSettingItem(()=>this.onClick(MORE_MENU.Remove_Key),
            '标签移除',
            require('./img/ic_remove.png'),
            {tintColor: this.props.theme.themeColor},
            null)}
          <View style={GlobalStyles.line}/>
          {/* 设置 */}
          <Text style={styles.groupTitle}>设置</Text>
          <View style={GlobalStyles.line}/>
          {ViewUtils.getSettingItem(()=>this.onClick(MORE_MENU.Custom_Theme),
            '自定义主题',
            require('./img/ic_custom_theme.png'),
            {tintColor: this.props.theme.themeColor},
            null)}
          <View style={GlobalStyles.line}/>
          {ViewUtils.getSettingItem(()=>this.onClick(MORE_MENU.About_Author),
            '关于作者',
            require('./img/ic_insert_emoticon.png'),
            {tintColor: this.props.theme.themeColor},
            null)}
          <View style={GlobalStyles.line}/>
        </ScrollView>
        {this.renderCustomThemeView()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    height: 90,
    backgroundColor: 'white',
  },
  groupTitle: {
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 5,
    fontSize: 12,
    color: 'gray',
  },
  image: {
    height: 22,
    width: 22,
  },
});
