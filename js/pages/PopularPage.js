import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  ListView,
  RefreshControl,
  DeviceEventEmitter,
  TouchableOpacity,
  FlatList
} from 'react-native';

import NavigationBar from '../common/NavigationBar';
import TabNavigator from 'react-native-tab-navigator';
import DataRepository, { FLAG_STORAGE } from '../expand/dao/DataRepository';
import ScrollableTabView, {ScrollableTabBar} from 'react-native-scrollable-tab-view';
import RepositoryCell from '../common/RepositoryCell';
import LanguageDao, {FLAG_LANGUAGE} from '../expand/dao/LanguageDao';
import ProjectModel from '../model/ProjectModel';
import FavoriteDao from '../expand/dao/FavoriteDao';
import Utils from '../utils/Utils';
import ActionUtils from '../utils/ActionUtils';
import SearchPage from './SearchPage';
import MoreMenu, {MORE_MENU} from '../common/MoreMenu';
import ViewUtils from '../utils/ViewUtils';
import { FLAG_TAB } from './HomePage';
import BaseComponent from './BaseComponent';
import CustomThemePage from './my/CustomTheme';
import NavigatorUtil from '../utils/NavigatorUtil';

const URL = 'https://api.github.com/search/repositories?q=';
const QUERY_STR = '&sort=stars';

var favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_popular);
var dataRepository = new DataRepository(FLAG_STORAGE.flag_popular);


export default class PopularPage extends BaseComponent {
    constructor(props) {
        super(props);
        this.languageDao = new LanguageDao(FLAG_LANGUAGE.flag_key);
        this.state = {
            languages: [],
            theme: this.props.theme,
            customThemeViewVisible: false,
        };
    }

    componentDidMount() {
        super.componentDidMount();
        this.loadLanguage();
    }

    loadLanguage() {
        this.languageDao.fetch()
        .then((languages)=>{
            this.setState({
                languages: languages
            })
        })
        .catch((err)=>{
            console.log(err)
        })
    }

    renderRightButton() {
        return <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
                onPress={()=>{
                    NavigatorUtil.goToSearchPage(this.props);
                }}
            >
                <View style={{padding: 5, marginRight: 8}}>
                    <Image source={require('../../res/images/ic_search_white_48pt.png')}
                        style={{width:24, height:24}}
                    ></Image>
                </View>
            </TouchableOpacity>
            {ViewUtils.getMoreButton(()=>this.refs.moreMenu.open())}
        </View>
    }

    renderMoreView() {
        let params = {...this.props, fromPage: FLAG_TAB.flag_favoriteTab}
        return <MoreMenu
            ref='moreMenu'
            {...params}
            menus={[
                MORE_MENU.Custom_Key,
                MORE_MENU.Sort_Key,
                MORE_MENU.Remove_Key,
                MORE_MENU.Custom_Theme,
                MORE_MENU.About_Author,
                MORE_MENU.About
            ]}
            onMoreMenuSelect={(e)=>{
                if (e===MORE_MENU.Custom_Theme) {
                    this.setState({
                        customThemeViewVisible: true,
                    })
                }
            }}
        />
    }

    renderCustomThemeView() {
        return (
          <CustomThemePage
            visible={this.state.customThemeViewVisible}
            {...this.props}
            onClose={()=>this.setState({
              customThemeViewVisible: false,
            })}
          ></CustomThemePage>
        );
    }

    render() {
        var statusBar = {
            backgroundColor: this.state.theme.themeColor,
        }
        let navigationBar = 
            <NavigationBar
                title={'最热'}
                statusBar={statusBar}
                style = {this.state.theme.styles.navBar}
                rightButton={this.renderRightButton()}
            />;
        let content = this.state.languages.length>0?
        <ScrollableTabView
                tabBarBackgroundColor={this.state.theme.themeColor}
                tabBarActiveTextColor='white'
                tabBarInactiveTextColor='mintcream'
                tabBarUnderlineStyle={{backgroundColor:'#e7e7e7', height: 2}}
                renderTabBar={()=><ScrollableTabBar/>}
            >
                {this.state.languages.map((language,i,arr)=>{
                    return language.checked?<PopularTab key={i} tabLabel={language.name} {...this.props}>{language.path}</PopularTab>:null;
                })}
            </ScrollableTabView>:null;
        return <View style={styles.container}>
            {navigationBar}
            {content}
            {this.renderMoreView()}
            {this.renderCustomThemeView()}
        </View>;
    }
}

class PopularTab extends BaseComponent {
    constructor(props) {
        super(props);
        this.isFavoriteChanged=false;
        this.state = {
            projectModels: [],
            isLoading: false,
            favoriteKeys: [],
            theme: this.props.theme,
        };
    }

    componentDidMount() {
        super.componentDidMount();
        this.loadData();
        this.listener = DeviceEventEmitter.addListener('favoriteChanged_popular', ()=>{
            this.isFavoriteChanged=true;
        })
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        if (this.listener) {
            this.listener.remove();
        }
    }

    onTabSelected(from, to) {
        if (to === FLAG_TAB.flag_popularTab && this.isFavoriteChanged) {
            this.isFavoriteChanged = false;
            this.getFavoriteKeys();
        }
    }

    // componentWillReceiveProps(nextProps) {
    //     if (this.isFavoriteChanged) {
    //         this.isFavoriteChanged=false;
    //         this.getFavoriteKeys();
    //     } else if(nextProps.theme !== this.state.theme){
    //         this.updateState({
    //             theme: nextProps.theme,
    //         });
    //         this.flushFavoriteState();
    //     }
    // }

    genFetchUrl(key) {
        return URL + key + QUERY_STR;
    }

    loadData() {
        this.updateState({
            isLoading: true
        })
        let url=this.genFetchUrl(this.props.tabLabel);
        dataRepository.fetchRepository(url)
        .then(result=> {
            this.items=result && result.items ? result.items : result ? result : [];
            this.getFavoriteKeys();
            if (result && result.update_date && !Utils.checkDate(result.update_date))
                return dataRepository.fetchNetRepository(url);
        })
        .then((items)=> {
            if (!items || items.length === 0)return;
            this.items = items;
            this.getFavoriteKeys();
        })
        .catch(error=> {
            console.log(error);
            this.updateState({
                isLoading: false
            });
        })
    }

    updateState(dic) {
        if (!this) return;
        this.setState(dic);
    }

    /**
     * 获取本地用户收藏的ProjectItem
     */
    getFavoriteKeys() {
        favoriteDao.getFavoriteKeys()
        .then((keys)=>{
            if(keys){
                this.updateState({favoriteKeys: keys});
            }
            this.flushFavoriteState();
        })
        .catch((error)=>{
            this.flushFavoriteState();
            console.log(error);
        })
    }

    /**
     * 更新ProjectItem的Favorite状态
     */
    flushFavoriteState() {
        let projectModels = [];
        let items = this.items;
        for(var i = 0, len = items.length; i < len; i++) {
            projectModels.push(new ProjectModel(items[i], Utils.checkFavorite(items[i], this.state.favoriteKeys)));
        }
        this.updateState({
            isLoading: false,
            projectModels: projectModels,
        });
    }

    onUpdateFavorite() {
        this.getFavoriteKeys();
    }

    renderItem(data) {
        const projectModel = data.item;
        return <RepositoryCell 
            key={projectModel.item.url} 
            projectModel={projectModel}
            theme={this.props.theme}
            onSelect={()=>ActionUtils.onSelectRepository({
                projectModel: projectModel,
                flag: FLAG_STORAGE.flag_popular,
                ...this.props,
                onUpdateFavorite:()=>this.onUpdateFavorite(),
            })}
            onFavorite={(item, isFavorite)=>ActionUtils.onFavorite(favoriteDao, item, isFavorite)}
        />
    }

    render() {
        return <View style={styles.container}>
            <FlatList
                data={this.state.projectModels}
                renderItem={(data)=>this.renderItem(data)}
                keyExtractor={item=>item.item.id+''}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.isLoading}
                        onRefresh={()=>this.loadData()}
                        colors={['#2196F3']}
                        tintColor={this.props.theme.themeColor}
                        title='loading...'
                        titleColor={this.props.theme.themeColor}
                    />
                }
            ></FlatList>
        </View>
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

