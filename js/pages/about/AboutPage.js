import React, { Component } from 'react';
import {
  View,
  Linking
} from 'react-native';

import ViewUtils from '../../utils/ViewUtils';
import { MORE_MENU } from '../../common/MoreMenu';
import GlobalStyles from '../../../res/styles/GlobalStyles'
import AboutCommon, { FLAG_ABOUT } from './AboutCommon';
import config from '../../../res/data/config';
import NavigatorUtil from '../../utils/NavigatorUtil';

export default class AboutPage extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.aboutCommon = new AboutCommon({...this.props, navigation: this.props.navigation}, (dic)=>this.updateState(dic), FLAG_ABOUT.flag_about, config);
    this.state = {
      projectModel: [],
      author:config.author,
      theme: this.params.theme,
    }
  }

  updateState(dic) {
    this.setState(dic)
  }

  componentDidMount() {
    this.aboutCommon.componentDidMount();
  }

  onClick(tab) {
    let TargetComponent, params = {...this.params,menuType:tab};
    switch (tab) {
      case MORE_MENU.About_Author:
        TargetComponent = 'AboutMePage';
        break;
      case MORE_MENU.WebSite:
        TargetComponent = 'WebViewPage';
        params.url = 'https://m.bilibili.com/index.html'; 
        params.title = 'BiliBili'
        break;
      case MORE_MENU.Feedback:
        var url = 'mailto://getinlight@gmail.com';
        Linking.canOpenURL(url).then(supported => {
          if (!supported) {
            console.log("can't handle url: "+url);
          } else {
            return Linking.openURL(url);
          }
        }).catch(err => console.log("An error occurred", err));
        break;
    }
    if(TargetComponent) {
      // this.props.navigator.push({
      //   component: TargetComponent,
      //   params: params,
      // });
      NavigatorUtil.gotoMenuPage(params, TargetComponent);
    }
  }

  render() {
    let content = <View>
      {this.aboutCommon.renderRepository(this.state.projectModels)}
      {ViewUtils.getSettingItem(
        ()=>this.onClick(MORE_MENU.About_Author),
        '关于作者',
        require('../../../res/images/ic_computer.png'),
        {tintColor: this.params.theme.themeColor},
      )}
      <View style={GlobalStyles.line}/>
      {ViewUtils.getSettingItem(
        ()=>this.onClick(MORE_MENU.WebSite),
        '网址',
        require('../my/img/ic_insert_emoticon.png'),
        {tintColor: this.params.theme.themeColor},
      )}
      <View style={GlobalStyles.line}/>
      {ViewUtils.getSettingItem(
        ()=>this.onClick(MORE_MENU.Feedback),
        '反馈',
        require('../../../res/images/ic_feedback.png'),
        {tintColor: this.params.theme.themeColor},
      )}
    </View>
    return this.aboutCommon.render(content, {
      name: '允儿',
      description: '这是一个用来查询Github最受欢迎与最热项目的APP',
      avatar: 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=697792124,3440814074&fm=26&gp=0.jpg',
      backgroundImg: 'http://pic3.16pic.com/00/03/88/16pic_388730_b.jpg',
    });
  }
}
