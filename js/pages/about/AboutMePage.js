import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableHighlight,
  Dimensions,
  ListView,
  Platform,
  Linking,
  Clipboard
} from 'react-native';

import ViewUtils from '../../utils/ViewUtils';
import { MORE_MENU } from '../../common/MoreMenu';
import GlobalStyles from '../../../res/styles/GlobalStyles'
import AboutCommon, { FLAG_ABOUT } from './AboutCommon';
import WebViewPage from '../WebViewPage';
import config from '../../../res/data/config';
import Toast, { DURATION } from 'react-native-easy-toast';
import NavigatorUtil from '../../utils/NavigatorUtil';

const FLAG = {
  REPOSITORY: '开源项目',
  BLOG: {
      name: '技术博客',
      items: {
          PERSONAL_BLOG: {
              title: '个人博客',
              url: 'http://jiapenghui.com',
          },
          CSDN: {
              title: 'CSDN',
              url: 'http://blog.csdn.net/fengyuzhengfan',
          },
          JIANSHU: {
              title: '简书',
              url: 'http://www.jianshu.com/users/ca3943a4172a/latest_articles',
          },
          GITHUB: {
              title: 'GitHub',
              url: 'https://github.com/crazycodeboy',
          },
      }
  },
  CONTACT: {
      name: '联系方式',
      items: {
          QQ: {
              title: 'QQ',
              account: '1586866509',
          },
          Email: {
              title: 'Email',
              account: 'crazycodeboy@gmail.com',
          },
      }
  },
  QQ: {
      name: '技术交流群',
      items: {
          MD: {
              title: '移动开发者技术分享群',
              account: '335939197',
          },
          RN: {
              title: 'React Native学习交流群',
              account: '165774887',
          }
      },
  },

};

export default class AboutMePage extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.aboutCommon = new AboutCommon({...this.props, navigation: this.props.navigation}, (dic)=>this.updateState(dic), FLAG_ABOUT.flag_about_me, config);
    this.state = {
      projectModel: [],
      author: config.author,
      showRepository: false,
      showBlog: false,
      showQQ: false,
      showContact: false,
      theme: this.params.theme,
    }
  }

  updateState(dic) {
    this.setState(dic)
  }

  componentDidMount() {
    this.aboutCommon.componentDidMount();
  }

  onClick(tab) {
    let TargetComponent, params = {...this.params,menuType:tab};
    switch (tab) {
      case FLAG.BLOG:
        this.updateState({showBlog: !this.state.showBlog});
        break;
      case FLAG.REPOSITORY:
        this.updateState({showRepository: !this.state.showRepository});
        break;
      case FLAG.QQ:
        this.updateState({showQQ: !this.state.showQQ});
        break;
      case FLAG.CONTACT:
        this.updateState({showContact: !this.state.showContact});
        break;
      case FLAG.CONTACT.items.QQ:
        Clipboard.setString(tab.account);
        this.toast.show('QQ: '+tab.account+' 已复制到剪切板');
        break;
      case FLAG.CONTACT.items.Email:
        var url = 'mailto://'+tab.account;
        Linking.canOpenURL(url).then(supported => {
          if (!supported) {
            console.log("can't handle url: "+url);
          } else {
            return Linking.openURL(url);
          }
        }).catch(err => console.log("An error occurred", err));
        break;
      case FLAG.QQ.items.MD:
        Clipboard.setString(tab.account);
        this.toast.show('QQ: '+tab.account+' 已复制到剪切板');
        break;
      case FLAG.QQ.items.RN:
        Clipboard.setString(tab.account);
        this.toast.show('QQ: '+tab.account+' 已复制到剪切板');
        break;
      case FLAG.BLOG.items.PERSONAL_BLOG:
        TargetComponent = 'WebViewPage';
        params.url = tab.url; 
        params.title = tab.title;
        break;
      case FLAG.BLOG.items.CSDN:
        TargetComponent = 'WebViewPage';
        params.url = tab.url; 
        params.title = tab.title;
        break;
      case FLAG.BLOG.items.JIANSHU:
        TargetComponent = 'WebViewPage';
        params.url = tab.url; 
        params.title = tab.title;
        break;
      case FLAG.BLOG.items.GITHUB:
        TargetComponent = 'WebViewPage';
        params.url = tab.url; 
        params.title = tab.title;
        break;
    }
    if (TargetComponent) {
      // this.props.navigator.push({
      //   component: TargetComponent,
      //   params: params,
      // });
      NavigatorUtil.gotoMenuPage(params, TargetComponent);
    }
  }

  /**
   * 获取item右侧图标
   * @param {*} isShow 
   */
  getClickIcon(isShow) {
    return isShow? require('../../../res/images/ic_tiaozhuan_up.png'): require('../../../res/images/ic_tiaozhuan_down.png');
  }

  /**
   * 显示列表数据
   * @param {*} dic 
   * @param {*} isShowAccount 
   */
  renderItems(dic, isShowAccount) {
    if (!dic) return null;
    let views = [];
    for (let i in dic) {
      let title = isShowAccount ? dic[i].title+': '+dic[i].account: dic[i].title;
      views.push(
        <View key={i}>
          {ViewUtils.getSettingItem(()=>this.onClick(dic[i]), title, null, {tintColor: this.params.theme.themeColor})}
          <View style={GlobalStyles.line}/>
        </View>
      );
    }
    return views;
  }

  render() {
    let content = <View>
      {ViewUtils.getSettingItem(
        ()=>this.onClick(FLAG.BLOG),
        FLAG.BLOG.name,
        require('../../../res/images/ic_computer.png'),
        {tintColor: this.params.theme.themeColor},
        this.getClickIcon(this.state.showBlog)
      )}
      <View style={GlobalStyles.line}/>
      {this.state.showBlog? this.renderItems(FLAG.BLOG.items): null}

      {ViewUtils.getSettingItem(
        ()=>this.onClick(FLAG.REPOSITORY),
        FLAG.REPOSITORY,
        require('../../../res/images/ic_code.png'),
        {tintColor: this.params.theme.themeColor},
        this.getClickIcon(this.state.showRepository)
      )}
      <View style={GlobalStyles.line}/>
      {this.state.showRepository? this.aboutCommon.renderRepository(this.state.projectModels): null}
      
      {ViewUtils.getSettingItem(
        ()=>this.onClick(FLAG.QQ),
        FLAG.QQ.name,
        require('../../../res/images/ic_computer.png'),
        {tintColor: this.params.theme.themeColor},
        this.getClickIcon(this.state.showQQ)
      )}
      <View style={GlobalStyles.line}/>
      {this.state.showQQ? this.renderItems(FLAG.QQ.items, true): null}
      
      {ViewUtils.getSettingItem(
        ()=>this.onClick(FLAG.CONTACT),
        FLAG.CONTACT.name,
        require('../../../res/images/ic_contacts.png'),
        {tintColor: this.params.theme.themeColor},
        this.getClickIcon(this.state.showContact)
      )}
      <View style={GlobalStyles.line}/>
      {this.state.showContact? this.renderItems(FLAG.CONTACT.items, true): null}
    </View>
    return <View style={styles.container}>
      {this.aboutCommon.render(content, this.state.author)}
      <Toast ref={e=>this.toast=e}></Toast>
    </View>
  }
}

const styles=StyleSheet.create({
  container: {
    flex: 1,
  },
})
