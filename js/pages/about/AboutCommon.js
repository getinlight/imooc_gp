import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Platform
} from 'react-native';

import ParallaxScrollView from 'react-native-parallax-scroll-view';
import ViewUtils from '../../utils/ViewUtils';
import FavoriteDao from '../../expand/dao/FavoriteDao';
import { FLAG_STORAGE } from '../../expand/dao/DataRepository';
import Utils from '../../utils/Utils';
import RepositoryCell from '../../common/RepositoryCell';
import RepositoryUtils from '../../expand/dao/RepositoryUtils';
import ActionUtils from '../../utils/ActionUtils';
import BackPressComponent from '../../common/BackPressComponent'
import NavigatorUtil from '../../utils/NavigatorUtil';

export var FLAG_ABOUT = { flag_about: 'about', flag_about_me: 'about_me' }

export default class AboutCommon {
  constructor(props, updateState, flag_about, config) {
    this.props = props;
    this.params = this.props.navigation.state.params;
    this.backPress=new BackPressComponent({backPress:(e)=>this.onBackPress(e)});
    this.updateState = updateState;
    this.flag = flag_about;
    this.config = config;
    this.respositories = [];
    this.favoriteKeys = null;
    this.favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_popular);
    this.repositoryUtils = new RepositoryUtils(this);
  }

  onBackPress(e){
    NavigatorUtil.goBack(this.props.navigation);
    return true;
}

  componentDidMount() {
    this.backPress.componentDidMount();
    if (this.flag === FLAG_ABOUT.flag_about) {
      this.repositoryUtils.fetchRepository(this.config.info.currentRepoUrl);
    } else {
      var urls = [];
      var items = this.config.items;
      for (let i = 0, l = items.length; i<l; i++) {
        urls.push(this.config.info.url+items[i]);
      }
      this.repositoryUtils.fetchRepositories(urls);
    }
  }

  componentWillUnmount(){
    this.backPress.componentWillUnmount();
  }

  /**
   * 通知数据发生改变
   * @param {*} items 改变的数据
   */
  onNotifyDataChanged(items) {
    this.updateFavorite(items);
  }

  /**
   * 更新项目的用户收藏状态
   * @param {*} respositories 
   */
  async updateFavorite(respositories) {
    if (respositories) this.respositories = respositories;
    if (!this.respositories) return;
    if (!this.favoriteKeys) {
      this.favoriteKeys = await this.favoriteDao.getFavoriteKeys();
    }
    let projectModels = [];
    for (var i = 0, len = this.respositories.length; i < len; i++) {
      var data = this.respositories[i];
      data = data.item?data.item:data;
      projectModels.push({
        isFavorite: Utils.checkFavorite(data, this.favoriteKeys ? this.favoriteKeys : []),
        item: data,
      });
    }
    this.updateState({
      projectModels: projectModels
    })
  }

onUpdateFavorite() {
  this.getFavoriteKeys();
}

/**
* 获取本地用户收藏的ProjectItem
*/
getFavoriteKeys() {
  this.favoriteDao.getFavoriteKeys()
  .then((keys)=>{
      if(keys){
          this.updateState({favoriteKeys: keys});
      }
  })
  .catch((error)=>{

  })
}

  /**
   * 创建项目视图
   * @param {*} projectModels 
   */
  renderRepository(projectModels) {
    if (!projectModels || projectModels.length === 0) return null;
    let views = [];
    for (let i = 0, l = projectModels.length; i<l; i++) {
      let projectModel = projectModels[i];
      views.push(
        <RepositoryCell 
            key={projectModel.item.url} 
            projectModel={projectModel}
            theme={this.params.theme}
            onSelect={()=>ActionUtils.onSelectRepository({
              projectModel: projectModel,
              parentComponent: this,
              flag: FLAG_STORAGE.flag_popular,
              ...this.params,
              onUpdateFavorite:()=>this.onUpdateFavorite(),
            })}
            onFavorite={(item, isFavorite)=>ActionUtils.onFavorite(this.favoriteDao, item, isFavorite, FLAG_STORAGE.flag_popular)}
        />
      )
    }
    return views;
  }

  getParallaxRenderConfig(params) {
    let config = {};
    config.renderBackground = () => (
      <View key="background">
        <Image source={{
          uri: params.backgroundImg,
          width: window.width,
          height: PARALLAX_HEADER_HEIGHT
        }} />
        <View style={{
          position: 'absolute',
          top: 0,
          width: window.width,
          backgroundColor: 'rgba(0,0,0,.4)',
          height: PARALLAX_HEADER_HEIGHT
        }} />
      </View>
    )
    config.renderForeground = () => (
      <View key="parallax-header" style={styles.parallaxHeader}>
        <Image style={styles.avatar} source={{
          uri: params.avatar,
          width: AVATAR_SIZE,
          height: AVATAR_SIZE
        }} />
        <Text style={styles.sectionSpeakerText}>
          {params.name}
        </Text>
        <Text style={styles.sectionTitleText}>
          {params.description}
        </Text>
      </View>
    )
    config.renderStickyHeader = () => (
      <View key="sticky-header" style={styles.stickySection}>
        <Text style={styles.stickySectionText}>{params.name}</Text>
      </View>
    )
    config.renderFixedHeader = () => (
      <View key="fixed-header" style={styles.fixedSection}>
        {ViewUtils.getLeftButton(() => NavigatorUtil.goBack(this.props.navigation))}
      </View>
    )
    return config;
  }

  render(content, params) {
    let renderConfig = this.getParallaxRenderConfig(params);
    return (
      <ParallaxScrollView
        headerBackgroundColor="#333"
        backgroundColor={this.params.theme.themeColor}
        stickyHeaderHeight={STICKY_HEADER_HEIGHT}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
        backgroundSpeed={10}
        {...renderConfig}
      >
        {content}
      </ParallaxScrollView>
    );
  }

}

const window = Dimensions.get('window');

const AVATAR_SIZE = 120;
const ROW_HEIGHT = 60;
const PARALLAX_HEADER_HEIGHT = 350;
const STICKY_HEADER_HEIGHT = 70;

const styles = StyleSheet.create({
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
  },
  stickySectionText: {
    color: 'white',
    fontSize: 20,
    margin: 10
  },
  fixedSection: {
    position: 'absolute',
    bottom: 0,
    right: 10,
    left: 0,
    top: 0,
    paddingRight: 8,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
    justifyContent: 'space-between',
  },
  fixedSectionText: {
    color: '#999',
    fontSize: 20
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    paddingTop: 100
  },
  avatar: {
    marginBottom: 10,
    borderRadius: AVATAR_SIZE / 2
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 18,
    paddingVertical: 5
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: 'white',
    borderColor: '#ccc',
    borderBottomWidth: 1,
    justifyContent: 'center'
  },
  rowText: {
    fontSize: 20
  }
});
