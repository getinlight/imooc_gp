import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  DeviceEventEmitter
} from 'react-native';
import TabNavigator from 'react-native-tab-navigator';
import PopularPage from './PopularPage';
import AsyncStorageTest from '../../AsyncStorageTest';
import MyPage from './my/MyPage';
import Toast, {DURATION} from 'react-native-easy-toast';
import WebViewTest from '../../WebViewTest';
import TrendingTest from '../../TrendingTest';
import TrendingPage from './TrendingPage';
import FavoritePage from './FavoritePage';
import BaseComponent from './BaseComponent';
import ThemeFactory, { ThemeFlags } from '../../res/styles/ThemeFactory';
import NavigatorUtil from '../utils/NavigatorUtil';

export const ACTION_HOME = {
  SHOW_TOAST:'showToast', 
  RESTART: 'restart', 
  THEME: 'theme',
  HOME_TAB_SELECT: 'home_tab_select'
};

export const FLAG_TAB =  {
  flag_popularTab: 'tb_popular',
  flag_trendingTab: 'tb_trending',
  flag_favoriteTab: 'tb_favorite',
  flag_my: 'tb_my',
}

export const EVENT_TYPE_HOME_TAB_SELECT = 'home_tab_select';

export default class HomePage extends BaseComponent {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    let selectedTab = this.params.selectedTab?this.params.selectedTab: 'tb_popular';
    this.state = {
      selectedTab: selectedTab,
      theme: this.params.theme||ThemeFactory.createTheme(ThemeFlags.Default),
    };
  }

  componentDidMount() {
    super.componentDidMount();
    this.listener = DeviceEventEmitter.addListener('ACTION_HOME', (action, params)=>this.onAction(action, params));
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    this.listener&&this.listener.remove();
  }

  /**
   * 通知回调事件处理
   * @param {*} action 
   * @param {*} params 
   */
  onAction(action, params) {
    if (ACTION_HOME.RESTART === action) {
      if(params)var {jumpToTab}=params;
      this.onRestart(jumpToTab);
    } else if (ACTION_HOME.SHOW_TOAST === action) {
      this.toast.show(params.text, DURATION.LENGTH_LONG);
    }
  }

  /**
   * 重启页面
   * @param {*} jumpToTab 默认显示的页面
   */
  onRestart(jumpToTab) {
    NavigatorUtil.resetToHomePage({
      ...this.params,
      selectedTab: jumpToTab,
      navigation: this.props.navigation,
    })
  }

  onTabClick(from, to) {
    this.setState({ selectedTab: to });
    DeviceEventEmitter.emit(EVENT_TYPE_HOME_TAB_SELECT, from, to);
  }

  _renderTab(Component, selectedTab, title, renderIcon) {
    return <TabNavigator.Item
      selected={this.state.selectedTab === selectedTab}
      selectedTitleStyle={this.state.theme.styles.selectedTitleStyle}
      title={title}
      renderIcon={() => <Image style={styles.image} source={renderIcon} />}
      renderSelectedIcon={() => <Image style={[styles.image, this.state.theme.styles.tabBarSelectedIcon]} source={renderIcon} />}
      onPress={() => this.onTabClick(this.state.selectedTab, selectedTab)}
    >
      <Component {...this.props} theme={this.state.theme}/>
    </TabNavigator.Item>
  }

  render() {
    return (
      <View style={styles.container}>
        <TabNavigator>
          {this._renderTab(PopularPage, FLAG_TAB.flag_popularTab, '最热', require('../../res/images/ic_polular.png'))}
          {this._renderTab(TrendingPage, FLAG_TAB.flag_trendingTab, '趋势', require('../../res/images/ic_trending.png'))}
          {this._renderTab(FavoritePage, FLAG_TAB.flag_favoriteTab, '收藏', require('../../res/images/ic_favorite.png'))}
          {this._renderTab(MyPage, FLAG_TAB.flag_my, '我的', require('../../res/images/ic_my.png'))}
        </TabNavigator>
        <Toast ref={toast=>this.toast=toast}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  page1: {
    flex: 1,
    backgroundColor: 'red',
  },
  page2: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  image: {
    height: 22,
    width: 22,
  },
});
