import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';

const FAVORITE_KEY_PREFIX = 'favorite_';

export default class FavoriteDao {
    constructor(flag) {
        this.flag = FAVORITE_KEY_PREFIX+flag;
    }

    /**
     * 保存收藏项目
     * @param {*} key 项目url
     * @param {*} value 收藏的项目
     * @param {*} callBack 
     */
    saveFavoriteItem(key, value, callBack) {
        AsyncStorage.setItem(key, value, (error)=>{
            if (!error) {
                this.updateFavoriteKeys(key, true);
            }
        })
    }

    /**
     * 更新 Favorite key 集合
     * @param {*} key 
     * @param {*} isAdd true 添加 false 删除
     */
    updateFavoriteKeys(key, isAdd) {
        AsyncStorage.getItem(this.flag, (error, result)=>{
            if(!error) {
                var favoriteKeys = [];
                if(result){
                    favoriteKeys = JSON.parse(result);
                }
                var index = favoriteKeys.indexOf(key);
                if(isAdd){
                    if(index===-1) favoriteKeys.push(key);
                } else {
                    if (index!==-1) favoriteKeys.splice(index, 1);
                }
                AsyncStorage.setItem(this.flag, JSON.stringify(favoriteKeys), (err)=>{
                    
                });
            }
        })
    }

    /**
     * 获取收藏项目对应的key
     */
    getFavoriteKeys() {
        return new Promise((resolve, reject)=>{
            AsyncStorage.getItem(this.flag, (error, result)=>{
                if(!error){
                    try {
                        resolve(JSON.parse(result));
                    } catch(e) {
                        reject(e);
                    }
                } else {
                    reject(error);
                }
            })
        })
    }

    removeFavoriteItem(key) {
        AsyncStorage.removeItem(key, (error)=>{
            if(!error){
                this.updateFavoriteKeys(key, false);
            }
        });
    }

    /**
     * 获取用户所收藏的项目
     */
    getAllItems() {
        return new Promise((resolve, reject)=>{
            this.getFavoriteKeys()
            .then(keys=>{
                var items = [];
                if (keys) {
                    AsyncStorage.multiGet(keys, (err, stores)=>{
                        try {
                            stores.map((store, i, stores)=>{
                                let value = store[0]
                                if (value) items.push(JSON.parse(store[1]));
                            })
                            resolve(items);
                        } catch(e) {
                            reject(e);
                        }
                    })
                } else {
                    resolve(items);
                }
            })
            .catch((e)=>{
                reject(e)
            })
        });
    }
    
}