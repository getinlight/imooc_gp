import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';

import Trending from 'GitHubTrending';

export var FLAG_STORAGE = {flag_popular:'popular', flag_trending: 'trending', flag_my: 'my'};

export default class DataRepository {

    constructor(flag) {
        this.flag = flag;
        if (flag === FLAG_STORAGE.flag_trending) {
            this.trending = new Trending();
        }
    }

    saveRepository(url, items, callBack) {
        if (!url || !items) return;
        let wrapData;
        if (this.flag === FLAG_STORAGE.flag_my) {
            wrapData = {item: items, updateDate: new Date().getTime()};
        } else {
            wrapData = {items: items, updateDate: new Date().getTime()};
        }
        AsyncStorage.setItem(url, JSON.stringify(wrapData), callBack);
    }

    fetchRepository(url) {
        return new Promise((resolve, reject)=>{
            //获取本地的数据
            this.fetchLocalRepository(url)
            .then((wrapData)=> {
                if (wrapData) {
                    resolve(wrapData, true);
                } else {
                    this.fetchNetRepository(url)
                    .then((data)=> {
                        resolve(data);
                    })
                    .catch((error)=> {
                        reject(error);
                    })
                }
            })
            .catch((error)=>{
                this.fetchNetRepository(url)
                .then((data)=> {
                    resolve(data);
                })
                .catch((error=> {
                    reject(error);
                }))
            })
        });
    }

    fetchLocalRepository(url) {
        return new Promise((resolve, reject)=>{
            AsyncStorage.getItem(url, (err, result)=>{
                if (!err) {
                    try {
                        resolve(JSON.parse(result));
                    } catch(e) {
                        reject(e);
                        console.log('fetchLocalRepository: '+e);
                    }
                } else {
                    reject(err);
                    console.log('fetchLocalRepository: '+e);
                }
            });
        });
    }

    fetchNetRepository(url) {
        return new Promise((resolve, reject)=>{
            if (this.flag !== FLAG_STORAGE.flag_trending) {
                fetch(url)
                .then((response)=>response.json())
                .catch((error)=>{
                    reject(error);
                })
                .then((responseData)=>{
                    // if (!responseData||!responseData.items) {
                    //     reject(new Error('responseData is null'));
                    //     return;
                    // }
                    // resolve(responseData.items);
                    // this.saveRepository(url,responseData.items)
                    if (this.flag === FLAG_STORAGE.flag_my && responseData) {
                        this.saveRepository(url, responseData);
                        resolve(responseData);
                    } else if (responseData && responseData.items) {
                        this.saveRepository(url, responseData.items);
                        resolve(responseData.items);
                    } else {
                        reject(new Error('responseData is null'));
                    }
                });
            } else {
                this.trending.fetchTrending(url)
                .then((items)=>{
                    if (!items) {
                        reject(new Error('responseData is null'));
                        return;
                    }
                    resolve(items);
                    this.saveRepository(url,items)
                })
                .catch((error)=>{
                    reject(error);
                })
            }
        });
    }

    
}