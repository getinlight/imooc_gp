import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';

import keysData from '../../../res/data/keys.json';
import langsData from '../../../res/data/langs.json';

export var FLAG_LANGUAGE = {flag_language: 'language_dao_language', flag_key: 'language_dao_key'};

export default class LanguageDao {
    constructor(flag) {
        this.flag = flag;
    }

    save(objectData){
        var stringData=JSON.stringify(objectData);
        AsyncStorage.setItem(this.flag, stringData, (error,result)=>{

        });
    }

    fetch() {
        return new Promise((resolve, reject)=>{
            AsyncStorage.getItem(this.flag, (err, result)=>{
                if (err) {
                    reject(err);
                    return;
                }
                if (!result) {
                    var data=this.flag===FLAG_LANGUAGE.flag_language?langsData:keysData;
                    this.save(data);
                    resolve(data);
                } else {
                    try {
                        resolve(JSON.parse(result));
                    } catch(e) {
                        reject(e);
                    }
                }
            })
        });
    }
}