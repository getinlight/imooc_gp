import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';

export default class ViewUtils {
    /**
     * 获取设置页的item
     * @param {*} callBack 单击item的回调方法
     * @param {*} text 显示文本
     * @param {*} icon 左侧图标
     * @param {*} tintStyle 图标颜色
     * @param {*} expandableIcon 右侧图标
     */
    static getSettingItem(callBack,text,icon,tintStyle,expandableIcon) {
        return <TouchableOpacity
            onPress={callBack}
        >
            <View style={styles.setting_item_container}>
              <View style={{flexDirection: 'row', alignItems:'center'}}>
                <Image source={icon}
                    resizeMode='stretch'
                    style={[{width:16, height:16, marginRight:10}, tintStyle]}
                />
                <Text>{text}</Text>
              </View>
              <Image source={expandableIcon?expandableIcon:require('../../res/images/ic_tiaozhuan.png')}
                style={[{marginRight: 10, height: 22, width: 22}, tintStyle]}
              />
            </View>
        </TouchableOpacity>
    }

    static getLeftButton(callBack)  {
        return <TouchableOpacity
            style={{padding: 8}}
            onPress={callBack}
        >
            <Image
                style={{width:26, height:26}}
                source={require('../../res/images/ic_arrow_back_white_36pt.png')}
            />
        </TouchableOpacity>;
    }

    static getRightButton(title, callBack) {
        return <TouchableOpacity
            style={{alignItems: 'center',}}
            onPress={callBack}>
            <View style={{marginRight: 10}}>
                <Text style={{fontSize: 20, color: '#FFFFFF',}}>{title}</Text>
            </View>
        </TouchableOpacity>
    }

    /**
     * 获取更多按钮
     * @param {*} callBack 
     */
    static getMoreButton(callBack) {
        return <TouchableHighlight
            underlayColor={'transparent'}
            ref='moreMenuButton'
            style={{padding: 5}}
            onPress={callBack}
        >
            <View style={{paddingRight: 8}}>
                <Image 
                    source={require('../../res/images/ic_more_vert_white_48pt.png')}
                    style={{width: 24, height: 24}}

                />
            </View>
        </TouchableHighlight>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF',
    },
    setting_item_container: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 10,
      height: 60,
      backgroundColor: 'white'
    },
  });