import React, {Component} from 'react';
import {
    Modal,
    TouchableOpacity,
    StyleSheet,
    Image,
    DeviceInfo,
    View,
    Text
} from 'react-native';

import PropTypes from 'prop-types';
import TimeSpan from '../model/TimeSpan';

export const TimeSpans = [
    new TimeSpan('Day', 'since=daily'),
    new TimeSpan('Week', 'since=weekly'),
    new TimeSpan('Month', 'since=monthly'),
];

export default class TrendingDialog extends Component {

    state = {
        visible: false,
    };

    show() {
        this.setState({
            visible: true,
        })
    }

    dismiss() {
        this.setState({
            visible: false,
        })
    }

    render() {
        let {onClose, onSelect} = this.props;
        return (
            <Modal
                transparent={true}
                visible={this.state.visible}
                onRequestClose={()=>onClose()}
            >
                <TouchableOpacity
                    style={styles.container}
                    onPress={()=>this.dismiss()}
                >
                    <Image
                        source={require('../../res/images/arrow_top.png')}
                        style={styles.arrow}
                    />
                    <View
                        style={styles.content}
                    >
                        {TimeSpans.map((result, i, arr)=>{
                            return <TouchableOpacity
                                key={i}
                                onPress={()=>{
                                    onSelect(arr[i])
                                }}
                                underlayColor={'transparent'}
                            >
                                <View
                                    style={{alignItems: 'center', flexDirection: 'row'}}
                                >
                                    <Text
                                        style={styles.text}
                                    >{'趋势+'+result.showText}</Text>
                                    {
                                        i!==result.length-1?<View
                                            style={styles.line}
                                        />:null
                                    }
                                </View>
                            </TouchableOpacity>
                        })}
                    </View>
                </TouchableOpacity>
            </Modal>
        );
    }

}

TrendingDialog.propTypes={
    onSelect: PropTypes.func.isRequired,
    onClose: PropTypes.func
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)',
        alignItems: 'center',
    },
    arrow: {
        marginTop: 56+(DeviceInfo.isIPhoneX_deprecated?24:0),
        width: 16,
        height: 6,
        resizeMode: 'contain'
    },
    text: {
        fontSize: 16,
        color: 'black',
        fontWeight: '400',
        paddingRight: 15,
        padding: 8,
        paddingLeft: 26,
        paddingRight: 26
    },
    line: {
        height: 0.3,
        backgroundColor: 'darkgray',
    },
    icon: {
        width: 16,
        height: 16,
        margin: 10,
        marginLeft: 15,
    },
    content: {
        backgroundColor: 'white',
        borderRadius: 3,
        paddingTop: 3,
        paddingBottom: 3,
        marginRight: 3,
    }
});