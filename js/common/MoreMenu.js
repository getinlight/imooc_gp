import React, { Component } from 'react';
import {
  Linking,
  ViewPropTypes
} from 'react-native';

import PropTypes from 'prop-types';
import MenuDialog from '../common/MenuDialog';
import NavigatorUtil from '../utils/NavigatorUtil';
import {FLAG_LANGUAGE} from '../expand/dao/LanguageDao';

/**
 * 更多菜单
 */
export const MORE_MENU = {
    // Custom_Language: {name: '自定义语言', icon: require('../page/my/img/ic_custom_language.png')},
    // Sort_Language: {name: '语言排序', icon: require('../page/my/img/ic_swap_vert.png')},
    // Custom_Theme: {name: '自定义主题', icon: require('../page/my/img/ic_view_quilt.png')},
    // Custom_Key: {name: '自定义标签', icon: require('../page/my/img/ic_custom_language.png')},
    // Sort_Key: {name: '标签排序', icon: require('../page/my/img/ic_swap_vert.png')},
    // Remove_Key: {name: '标签移除', icon: require('../page/my/img/ic_remove.png')},
    // About_Author: {name: '关于作者', icon: require('../page/my/img/ic_insert_emoticon.png')},
    // About: {name: '关于', icon: require('../../res/images/ic_trending.png')},
    // Website: {name: 'Website', icon: require('../../res/images/ic_computer.png')},
    // Feedback: {name: '反馈', icon: require('../../res/images/ic_feedback.png')},
    // Share: {name: '分享', icon: require('../../res/images/ic_share.png')},

    Custom_Language: {name: '自定义语言', icon: require('../../res/img/ic_custom_language.png')},
    Sort_Language: {name: '语言排序', icon: require('../../res/img/ic_swap_vert.png')},
    Custom_Theme: {name: '自定义主题', icon: require('../../res/img/ic_view_quilt.png')},
    Custom_Key: {name: '自定义标签', icon: require('../../res/img/ic_custom_language.png')},
    Sort_Key: {name: '标签排序', icon: require('../../res/img/ic_swap_vert.png')},
    Remove_Key: {name: '标签移除', icon: require('../../res/img/ic_remove.png')},
    About_Author: {name: '关于作者', icon: require('../../res/img/ic_insert_emoticon.png')},
    About: {name: '关于', icon: require('../../res/images/ic_trending.png')},
    Website: {name: 'Website', icon: require('../../res/images/ic_computer.png')},
    Feedback: {name: '反馈', icon: require('../../res/images/ic_feedback.png')},
    Share: {name: '分享', icon: require('../../res/images/ic_share.png')},

};

export default class MoreMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    static propTypes = {
        contentStyle: ViewPropTypes.style,
        menus: PropTypes.array.isRequired,
    }

    /**
     * 打开更多菜单
     */
    open() {
        this.showPopover();
    }

    showPopover() {
        this.dialog.show();
    }

    closePopover() {
        this.dialog.dismiss();
    }

    onMoreMenuSelect(tab) {
        this.closePopover();
        if (typeof(this.props.onMoreMenuSelect)=='function') {
            this.props.onMoreMenuSelect(tab);
        }
        let TargetComponent, params = {...this.props, menuType: tab}
        switch (tab) {
            case MORE_MENU.Custom_Language:
                TargetComponent = 'CustomKeyPage';
                params.flag = FLAG_LANGUAGE.flag_language;
                break;
            case MORE_MENU.Custom_Key:
                TargetComponent = 'CustomKeyPage';
                params.flag = FLAG_LANGUAGE.flag_key;
                break;
            case MORE_MENU.Remove_Key:
                TargetComponent = 'CustomKeyPage';
                params.flag = FLAG_LANGUAGE.flag_key;
                params.isRemoveKey = true;
                break;
            case MORE_MENU.Sort_Language:
                TargetComponent = 'SortKeyPage';
                params.flag = FLAG_LANGUAGE.flag_language;
                break;
            case MORE_MENU.Sort_Key:
                TargetComponent = 'SortKeyPage';
                params.flag = FLAG_LANGUAGE.flag_key;
                break;
            case MORE_MENU.Custom_Theme:
                break;
            case MORE_MENU.About_Author:
                TargetComponent = 'AboutMePage';
                break;
            case MORE_MENU.About:
                TargetComponent = 'AboutPage';
                break;
            case MORE_MENU.Feedback:
                var url = 'mailto://getinlight@gmail.com';
                Linking.canOpenURL(url).then(supported => {
                if (!supported) {
                    console.log("can't handle url: "+url);
                } else {
                    return Linking.openURL(url);
                }
                }).catch(err => console.log("An error occurred", err));
                break;
            case MORE_MENU.About:
                break;
        }

        if(TargetComponent) {
            NavigatorUtil.gotoMenuPage(params, TargetComponent);
        }
    }

    renderMoreView() {
        const {theme, menus} = this.props;
        return (
            <MenuDialog
                ref={dialog=>this.dialog=dialog}
                menus={menus}
                theme={theme}
                onSelect={(tab)=>this.onMoreMenuSelect(tab)}
            />
        );
    }

    render() {
        return this.renderMoreView();
    }

}